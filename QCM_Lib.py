
import json
import random
from struct import pack


def floattobin(f,format='>f'):
    """Retourne une chaine représentant le flottant en binaire"""
    assert format == '>f' or format=='>d'
    t = pack(format,f)
    s = ""
    for i in range(0,(8 if format=='>d'else 4)):
        s+="{:{fill}8b}".format(t[i], fill='0')
    return s

assert floattobin(1.125)=="00111111100100000000000000000000"


def sauver_json(nom_fic ,qcm):
    with open(nom_fic,"w", newline="", encoding="utf-8") as jsonfile: json.dump(qcm ,jsonfile)


def creer(nomQCM : str):
    """
    creer le questionnaire avec une liste pour les questions
    """
    assert isinstance(nomQCM, str), "l'argument doit être un str"
    QCM = {"nom":nomQCM, "questions":[]}
    return QCM

assert creer("toto") == {'nom': 'toto', 'questions': []}


def creer_question(texte,choix):
    """
    cree une question avec un dictionnaire contenant l'énoncé, une liste pour les réponses, le type
    """
    assert choix in ["multiple","ouverte"], "la question doit etre d'un des types suivants: multiple ou ouverte"
    question = {"enonce":texte,"reponses":[],"type":choix}
    return (question)

assert creer_question("machin", "multiple") == {'enonce': 'machin', 'reponses': [], 'type': 'multiple'}


def creer_reponse(type, proposition):
    """
    crée une réponse de type ouverte dans un dictionnaire
    """
    if type == 'ouverte':
        reponse = {"text": proposition}
    elif type == 'multiple':
        reponse = {"fraction": 100, "text": proposition}
    return (reponse)


def ajouter_reponse(question,reponse):
    """
    ajoute une réponse dans la liste
    """
    question["reponses"].append(reponse)
    return(question)

def ajouter_question(QCM,question):
    """
    ajoute une question dans la liste des questions du QCM
    """
    QCM["questions"].append(question)
    return(QCM)

def creer_question_automatic(choix):
    """
    cree une question générée automatiquement avec un dictionnaire contenant l'énoncé, la réponse générée, le type
    """
    if choix == "naturel/bin":
        naturel = random.randint(0,150)
        texte = "Donner la traduction en binaire de cette valeur donnée en décimal:"+str(naturel)
        reponse = bin(naturel)

    elif choix == "naturel/hexa":
        naturel = random.randint(0,150)
        texte = "Donner la traduction en hexadecimal de cette valeur donnée en décimal:"+str(naturel)
        reponse = hex(naturel)

    elif choix == "bin/naturel":
        naturel = random.randint(0,150)
        texte = "Donner la traduction en décimal de cette valeur donnée en binaire:"+str(bin(naturel))
        reponse = naturel

    elif choix == "hexa/naturel":
        naturel = random.randint(0,150)
        texte = "Donner la traduction en décimal de cette valeur donnée en hexadecimal:"+str(hex(naturel))
        reponse = naturel

    elif choix == "naturel/C2_8":
        naturel = -random.randint(0,100)
        reponse = "{0:{fill}8b}".format(naturel%256, fill='0')
        texte = "Donner le code en C2 sur 8 bits de cette valeur:"+str(naturel)

    elif choix == "float/IEEE":
        float = random.randint(0,150)/(2**4)
        texte = "Donner la traduction au format IEEE sur 32 bits de cette valeur donnée en float:"+str(float)
        reponse = floattobin(float,'>f')

    question = {"enonce": texte, "reponses": [{"text": reponse}], "type": "ouverte"}

    return (question)


def supprimer_question(QCM, numero):
    del QCM['questions'][numero-1]
    return QCM


def modifier_question(QCM,numero, nouvel_enonce):
    QCM['questions'][numero-1]['enonce'] = nouvel_enonce
    return QCM


def modifier_reponse(QCM, numero_question, numero_reponse, nouvel_enonce):
    QCM['questions'][numero_question-1]['reponses'][numero_reponse-1]['text'] = nouvel_enonce
    return QCM


def supprimer_reponse(QCM, numero_question, numero_reponse):
    del QCM['questions'][numero_question-1]['reponses'][numero_reponse-1]
    return QCM