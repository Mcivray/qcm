
from QCM_Lib import *

"""
Création question 0, la Tour Eiffel
"""

question0=creer_question("Où se trouve la Tour Eiffel ?","multiple")

rep0=creer_reponse_multiple(0,"A Londres")
ajouter_reponse(question0,rep0)

rep1=creer_reponse_multiple(100,"A Paris")
ajouter_reponse(question0,rep1)

rep2=creer_reponse_multiple(0,"A Rome")
ajouter_reponse(question0,rep2)

QCM=creer("toto")
ajouter_question(QCM,question0)

"""
Création question 1, combien font 2+2
"""

question1=creer_question("Combien font 2+2 ?","ouverte")
rep0=creer_reponse_ouverte("4")
ajouter_reponse(question1,rep0)
ajouter_question(QCM,question1)

"""
Création question 2, conversion float en IEEE
"""

question2=creer_question_automatic("float/IEEE")
ajouter_question(QCM,question2)


"""
Création question 3, conversion float en IEEE
"""

question3=creer_question_automatic("naturel/C2_8")
ajouter_question(QCM,question3)



print(QCM)

#sauver_json("QCM.json" ,QCM)



modifier_question(QCM,2,'6+7')
modifier_reponse(QCM,2,1,10)
print(QCM)
supprimer_reponse(QCM,1,2)
print(QCM)