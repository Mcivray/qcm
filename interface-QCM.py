
import tkinter as tk
from tkinter import *
from QCM_Lib import *
from tkinter.filedialog import *
from functools import partial

# Création fenêtre
fenetre = Tk()
fenetre.geometry("1200x500+300+300".format(fenetre.winfo_screenwidth(), fenetre.winfo_screenheight()))
fenetre.title("QCM")

menuBar = Menu(fenetre)


def mise_aj():
    """
    creation du QCM
    """
    global QCM
    global menubar
    global dernierQCM
    nomQCM = champnomQCM.get()
    print(nomQCM)
    try:
        dernierQCM = QCM['nom']
    except:
        dernierQCM = nomQCM
    QCM = creer(nomQCM)
    print(QCM)
    mise_a_jour_menu()


def mise_ajqauto():
    """
    appel de la procédure de création des questions automatiques
    """
    v = choix_type_auto.get()
    choix = ["naturel/bin", "naturel/hexa", "bin/naturel", "hexa/naturel", "naturel/C2_8", "float/IEEE" ]
    ajouter_question(QCM, creer_question_automatic(choix[v]))
    mise_a_jour_menu()
    print(QCM)


def mise_ajq():
    """
    appel de la procédure de création des questions
    """
    v = choix_type.get()
    choix = ["ouverte", "multiple"]
    texte = champquestion.get()
    ajouter_question(QCM, creer_question(texte, choix[v]))
    mise_a_jour_menu()
    print(QCM)


def inserer_reponse(question, reponse):
    ajouter_reponse(question, reponse)
    mise_a_jour_menu()
    print(QCM)


def mise_a_jour_menu():
    """
    apres mise à jour du QCM, cette fonction est appelée afin de créer un menu dont la structure est calquée sur celle du QCM
    """
    global QCM
    global dernierQCM
    menuQuestion = Menu(menuBar, tearoff=0)
    cpt=0
    for i in QCM['questions']:
        cpt+=1
        cpt2=0
        menuReponse = Menu(menuQuestion, tearoff=0)
        if i['type'] == 'multiple' or i['reponses'] == []: #réponse unique si pas multiple
            reponse_vide = creer_reponse(i['type'], 'à saisir')
            menuReponse.add_command(label="Ajouter une réponse", command=partial(inserer_reponse,i,reponse_vide))
            menuReponse.add_separator()
        for j in i['reponses']:
            cpt2 += 1
            menuReponse.add_command(label="Réponse" + str(cpt2), command=partial(Saisie_txt, cpt-1, cpt2-1))
        menuQuestion.add_cascade(label="Question"+str(cpt), menu=menuReponse, command=partial(Saisie_txt_q, cpt - 1))
        menuQuestion.add_separator()
    try:
        menuBar.delete(dernierQCM)
        dernierQCM=QCM['nom']
    except:
        pass
    menuBar.add_cascade(label=QCM['nom'], menu=menuQuestion)
    fenetre.config(menu=menuBar)


def recuperer_txt(i, j, f, poids):
    """
    permet de récupérer le champ de saisie de poids pour une pop up de réponse multiple
    """
    inputValue = main_text.get(1.0, END)
    print(inputValue)
    QCM['questions'][i]['reponses'][j]['text'] = inputValue
    if poids == "fraction":
        fraction = champpoids.get()
        QCM['questions'][i]['reponses'][j]['fraction'] = fraction
    print(QCM)
    f.destroy()


def Saisie_txt(i, j):
    """
    permet de récupérer le champ de saisie texte pour une pop up de réponse multiple
    """
    ftxt = Toplevel()
    ftxt.geometry('800x200')
    ftxt.title('Reponse')
    ftxt.transient(fenetre)  # Réduction popup impossible
    label = Label(ftxt, text="Modifier ce texte:")
    label.pack()
    global main_text
    global champpoids
    main_text = Text(ftxt,height=5,width=100)
    main_text.insert(1.0, QCM['questions'][i]['reponses'][j]['text'])
    main_text.pack()
    main_text.focus_set()
    Button(ftxt, text='Quitter', command=ftxt.destroy).pack(padx=10, pady=10)
    if QCM['questions'][i]['type'] == 'multiple' :
        labelpoids = Label(ftxt, text="Saisir le poids de cette réponse :")
        labelpoids.pack(side="left")
        champpoids = Entry(ftxt, textvariable=IntVar())
        champpoids.delete(0, "end")
        champpoids.insert(0, QCM['questions'][i]['reponses'][j]['fraction'])
        champpoids.pack(side="left")
        button = Button(ftxt, text="valider", command=lambda: recuperer_txt(i, j, ftxt, "fraction"))
        button.pack(side="left")
    else :
        button = Button(ftxt, text="valider", command=lambda: recuperer_txt(i, j, ftxt,"pasfraction"))
        button.pack(side="left")
    fenetre.wait_window(ftxt)


def Saisie_txt_q(i):
    """
    permet de récupérer le champ de saisie texte pour une pop up de question
    """
    global ftxt
    ftxt = Toplevel()
    ftxt.geometry('600x300')
    ftxt.title('Question')
    ftxt.transient(fenetre)  # Réduction popup impossible
    label = Label(ftxt, text="Modifier ce texte:")
    label.pack()
    global main_text
    main_text = Text(ftxt,height=5,width=100)
    main_text.insert(1.0, QCM['questions'][i]['enonce'])
    main_text.pack()
    main_text.focus_set()
    buttonV = Button(ftxt, text="Valider", command=lambda: recuperer_txt_q(i, ftxt))
    buttonV.pack(padx=10, pady=10)
    Button(ftxt, text='Quitter', command=ftxt.destroy).pack(padx=10, pady=10)
    button = Button(ftxt, text="Supprimer cette question", command=lambda: supprimer_question(i, ftxt))
    button.pack(padx=10, pady=10)
    fenetre.wait_window(ftxt)


def recuperer_txt_q(i, f):
    """
     permet de récupérer le champ de saisie de texte pour une pop up de question
     """
    inputValue = main_text.get(1.0, END)
    print(inputValue)
    QCM['questions'][i]['enonce'] = inputValue
    print(QCM)
    f.destroy()

def supprimer_question(numero,f):
    del QCM['questions'][numero]
    mise_a_jour_menu()
    print(QCM)
    f.destroy()

def openFile():
    global QCM
    file = askopenfilename(title="Choose the file to open",
                           filetypes=[("Qcm json", ".json"), ("All files", ".*")])
    print(file)
    with open(file) as f:
        QCM = json.load(f)
        print(QCM)
        mise_a_jour_menu()

def saveFile():
    global QCM
    file = asksaveasfilename(title="Choose the file to save",
                           filetypes=[("Qcm json", ".json"), ("All files", ".*")])
    print(file)
    sauver_json(file, QCM)


# Créer frame fichier

labelframenomQCM = LabelFrame(fenetre, text="Fichier", padx="5", pady="5", relief="groove",borderwidth="5")
labelframenomQCM.pack(fill="x")

labelnomQCM = Label(labelframenomQCM, text="Saisir le nom du QCM :")
labelnomQCM.pack(side="left")
champnomQCM = Entry(labelframenomQCM, textvariable=StringVar())
champnomQCM.pack(side="left")
button = Button(labelframenomQCM, text="valider", command=mise_aj)
button.pack(side="left")
buttonQ = Button(labelframenomQCM, text='Quitter', command=fenetre.destroy)
buttonQ.pack(side="left", padx=10, pady=10 )


# Créer frame question

labelframequestion = LabelFrame(fenetre, text="Questions", padx="5", pady="5", relief="groove",borderwidth="5")
labelframequestion.pack(fill="x")
labelquestion = Label(labelframequestion, text="Enoncé d'une question ouverte/multiple :")
labelquestion.pack(side="left")
champquestion = Entry(labelframequestion, textvariable=StringVar())
champquestion.pack(side="left",fill = "x", expand= True)
button = Button(labelframequestion, text="valider", command=mise_ajq)
button.pack(side=RIGHT)
# Création du cadre pour selectionner le type.
cadreType = Frame(labelframequestion, borderwidth=2, relief=GROOVE)
# Affiche le cadreType.
cadreType.pack(side=BOTTOM, padx=10, pady=10)
# Affiche le titre dans le cadreType (qui sert de consigne au sein du cadre).
Label(cadreType, text="Choisir un type de question :").pack(padx=10, pady=10)
# Création des radio-boutons avec des valeurs pour choisir quel type choisir.
choix_type = IntVar(value=10)
bt1 = Radiobutton(cadreType, text="ouverte", variable=choix_type, value=0)
bt2 = Radiobutton(cadreType, text="multiple", variable=choix_type, value=1)
bt1.pack()
bt2.pack()


# Créer frame question automatique

labelframequestionauto = LabelFrame(fenetre, text="Questions automatiques", padx="5", pady="5", relief="groove",borderwidth="5")
labelframequestionauto.pack(fill="x")
bauto = Button(labelframequestionauto, text="creer une question", command=mise_ajqauto)
bauto.pack(side = LEFT)
# Création du cadre pour selectionner le type.
cadreType = Frame(labelframequestionauto, borderwidth=2, relief=GROOVE)
# Affiche le cadreType.
cadreType.pack(side=LEFT, padx=10, pady=10)
# Affiche le titre dans le cadreType (qui sert de consigne au sein du cadre).
Label(cadreType, text="Choisir un type de question :").pack(padx=10, pady=10)
# Création des radio-boutons avec des valeurs pour choisir quel type choisir.
choix_type_auto = IntVar(value = 10)
bt3 = Radiobutton(cadreType, text="naturel/bin", variable=choix_type_auto, value=0)
bt4 = Radiobutton(cadreType, text="naturel/hexa", variable=choix_type_auto, value=1)
bt5 = Radiobutton(cadreType, text="bin/naturel", variable=choix_type_auto, value=2)
bt6 = Radiobutton(cadreType, text="hexa/naturel", variable=choix_type_auto, value=3)
bt7 = Radiobutton(cadreType, text="naturel/C2_8", variable=choix_type_auto, value=4)
bt8 = Radiobutton(cadreType, text="float/IEEE", variable=choix_type_auto, value=5)
# Affichage des radio-boutons dans le "cadreType".
bt3.pack()
bt4.pack()
bt5.pack()
bt6.pack()
bt7.pack()
bt8.pack()


# Créer menu

filemenu = Menu(menuBar, tearoff=0)
filemenu.add_command(label="Open", command=openFile)
filemenu.add_command(label="Save", command=saveFile)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=fenetre.quit)
menuBar.add_cascade(label="File", menu=filemenu)
helpmenu = Menu(menuBar, tearoff=0)
helpmenu.add_command(label="Help Index")
helpmenu.add_command(label="About...")
menuBar.add_cascade(label="Help", menu=helpmenu)

fenetre.config(menu=menuBar)
fenetre.mainloop()  # Boucle principale




