function insere_nouvelle_question_ouverte(question,i){
    const formulaire = document.getElementById('quest');
    const para = document.createElement('p');
    const label = document.createElement('label');
    label.setAttribute('for', i);
    const enonce = document.createTextNode(question['enonce']);
    label.appendChild(enonce);
    para.appendChild(label);
    formulaire.appendChild(para);
    input = document.createElement('input');
    input.setAttribute('type', 'text');
    input.setAttribute('id', i);
    para.appendChild(input);
}

function insere_nouvelle_question_multiple(question,i){
	// on affiche l'énnoncé de la question
    const formulaire = document.getElementById('quest');
    const para = document.createElement('p');
    const label = document.createElement('label');
    label.setAttribute('for', i);
    const enonce = document.createTextNode(question['enonce']);
    label.appendChild(enonce);
    para.appendChild(label);
    formulaire.appendChild(para);
	for (let r=0;r<question['reponses'].length;r++){// pour chaque réponse possible on va créer une case a cocher
		input = document.createElement('input');
		input.setAttribute('type', 'checkbox');
		input.setAttribute('id', question['reponses'][r]['text']);
		//on donne a chaque case un nom et une valeur (réutilisés dans le score)
		input.setAttribute('name',question['reponses'][r]['text']);
		input.setAttribute('value',question['reponses'][r]['fraction']);
		//on crée le texte qui sera affiché à coté de la case
		let lab=document.createElement('label');
		let rep = document.createTextNode(question['reponses'][r]['text']);
		lab.appendChild(rep);
		para.appendChild(input);
		para.appendChild(lab);
	}	
}

function affiche(questions){
    for (let i=0;i<questions.length;i++){
        switch (questions[i]['type']) {
            case 'ouverte':
                insere_nouvelle_question_ouverte(questions[i],i+"");
                break;
            case 'multiple':
                insere_nouvelle_question_multiple(questions[i],i+"");
                break;
            default:
                console.log('Désolé, type ' + questions['type']  + 'inconnu !');
          }
    }
    const formulaire = document.getElementById('quest');
    const bouton = document.createElement('input');
    bouton.setAttribute('type', 'button');
    bouton.setAttribute('value', 'Envoyer réponses');
    bouton.onclick = function(){
		formulaire.removeChild(bouton)
        score(questions);
    }
    formulaire.appendChild(bouton);
}

function correction(questions, correct){
	
    for (let i=0;i<questions.length;i++){
		const affichage = document.getElementById('correc');
		const para = document.createElement('p');
		
		const label = document.createElement('label');
		
		const enonce = document.createTextNode(questions[i]['enonce']);
		label.appendChild(enonce);
		para.appendChild(label);
	
		var faux = document.createElement("img");
		faux.src = "faux.png";
		var vrai = document.createElement("img");
		vrai.src = "vrai.png";
		
		if (correct[i]==1){
			
			para.appendChild(vrai)
		}
		else{
			
			para.appendChild(faux)
		}	
		
		affichage.appendChild(para);
		
		
    }
    
}

function score(questions){
    let total = 0;
	let correct = new Array();// stock le résultat de chaque réponse
    for (let i=0;i<questions.length;i++){
        switch (questions[i]['type']) {
            case 'ouverte':
                const reponse_utilisateur = document.getElementById(i+"").value;
                if (questions[i]['reponses'][0]['text'] == reponse_utilisateur){
                    total += 1;
					// 1 = réponse bonne; par sécurité je met la valeur a 0 sinon
					correct[i]=1;
				}
				else
					correct[i]=0;		
			
                break;
            case 'multiple':
				let stotal=0;
				let erreur=0;
				for (let r=0;r<questions[i]['reponses'].length;r++){//passe en revu chaque réponse possible
					
					const reponse_utilisateur = document.getElementById(questions[i]['reponses'][r]['text']+"").value;//récupère la valeur de la réponse
					if (document.getElementById(questions[i]['reponses'][r]['text']+"").checked==true){
						if (reponse_utilisateur != 0)
							stotal += reponse_utilisateur;
						else
							erreur+=1;// une mauvaise case cochée = question ratée
					}
				}	
				
				//vérifie si toutes les bonnes réponses ont été cochées (on vérifie à moins de 100 pour les cas où le total des fractions seraient inférieur)
				if (stotal>90){
					
					if (erreur ==0){
						
						total +=1;
						correct[i]=1;
						
					}
					else
						correct[i]=0;
				}
				
                break;
            default:
                console.log('Désolé, type ' + questions['type']  + 'inconnu !');
          }
    }
	correction(questions, correct);
   
}




function chargement(){
	const choix_util = document.getElementById("nomqcm").value;
	fetch(choix_util+".json")
    .then(response => response.json())
    .then(json => affiche(json.questions));
}

/*demande à l'utilisateur quel qcm il veut charger puis lance la fonction chargement qui charge le qcm demandé
*/
window.onload = function(){
	const formulaire1 = document.getElementById('selection');
	const intro = document.createElement('p');
	const label1 = document.createElement('label');
    const enonce1 = document.createTextNode("entrer le nom de votre qcm");
    label1.appendChild(enonce1);
    intro.appendChild(label1);
	formulaire1.appendChild(intro);
	nomqcm = document.createElement('input');
    nomqcm.setAttribute('type', 'text');
    nomqcm.setAttribute('id', "nomqcm");
    intro.appendChild(nomqcm);
	
	
	
    const bouton1 = document.createElement('input');
    bouton1.setAttribute('type', 'button');
    bouton1.setAttribute('value', 'Valider');
    bouton1.onclick = function(){
		formulaire1.removeChild(bouton1)
        chargement();
    }
    formulaire1.appendChild(bouton1);
    
}